# learning-git

A sample repository to help git tutors and learners alike.

## Conventions

The CONTRIBUTORS.md file should be sorted alphabetically.

## License

This repository is licensed under the terms of the MIT License.
